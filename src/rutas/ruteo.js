"use strict";

(function () {
  function init() {
    var router = new Router([
      new Route("home", "inicio.html", true),
      new Route("about", "quiensomos.html"),
      new Route("workteam", "profesionales.html"),
      new Route("appointments", "cita.html"),
      new Route("inquiries", "consultar.html"),
      new Route("covid", "covid.html"),
      new Route("contactus", "contactenos.html"),
      new Route("manage", "admin.html"),
      new Route("inisesion", "acceso.html"),
      new Route("regis", "registro.html"),

      new Route("utlist", "tipousuario/tulista.html"),
      new Route("utadd", "tipousuario/tucrear.html"),
      new Route("utmanage", "tipousuario/tuadmin.html"),
      new Route("utdelete", "tipousuario/tueliminar.html"),
      new Route("utedit", "tipousuario/tueditar.html"),

      new Route("reest", "reestablecer.html"),
    ]);
  }
  init();
})();

// new Route('login', 'acceso.html'),
