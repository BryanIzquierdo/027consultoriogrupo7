function registrarUsu() {
    const nom = document.getElementById('nom').value;
    const ape = document.getElementById('ape').value;
    const tipd = document.getElementById('tipd').value;
    const tipu = document.getElementById('tipu').value;
    const doc = document.getElementById('doc').value;
    const cor = document.getElementById('cor').value;
    const tel = document.getElementById('tel').value;
    const usu = document.getElementById('usu').value;
    const cla = SHA512(document.getElementById('cla').value);

    let objetoEnviar = {
        tipoDocumentoUsuario: tipd,
        documentoUsuario: doc,
        nombreUsuario: nom,
        apellidoUsuario: ape,
        correoUsuario: cor,
        telefonoUsuario: tel,
        usuarioUsuario: usu,
        claveUsuario: cla,
        codTipoUsuario: {
            codTipoUsuario: tipu
        }
    };

    const apiCrearRol = "http://localhost:8094/consultorioOnline/usuario/crear";
    fetch(apiCrearRol, {
        method: "POST",
        body: JSON.stringify(objetoEnviar),
        headers: { "Content-type": "application/json; charset=UTF-8" }
    })
        .then(respuesta => respuesta.json())
        .then(datos => {
            if (datos.hasOwnProperty('codUsuario')) {
                // console.log(datos)
                document.getElementById("usuMsgOk").style.display = "";
                document.getElementById("usuMsgError").style.display = "none";
            } else {
                console.log("No se puede grabar -> " + datos.status);
                document.getElementById("usuMsgOk").style.display = "none";
                document.getElementById("usuMsgError").style.display = "";
            }

        })
        .catch(miError => console.log(miError));

    document.getElementById("formaCrearUsu").reset();
    document.getElementById("formaCrearUsu").classList.remove("was-validated");

}
