function obtenerTodosTipoUsuarioAdmin() {
    const apiObtenerRoles = "http://localhost:8094/backo27g7consultorio/tipousu/obtener";
    const miPromesaRoles = fetch(apiObtenerRoles)
        .then((respuesta) => respuesta.json());

    Promise.all([miPromesaRoles]).then((arregloDatos) => {
        const datos = arregloDatos[0];
        crearFilaTipoUsuAdmin(datos);
    }
    );
}

function confirmarEliminar(codRol) {
    if (window.confirm("¿Realmente desea eliminar el tipo de usuario?")) {
        window.location.replace("#utdelete/" + codRol);
    }
}

function crearFilaTipoUsuAdmin(arrObjeto) {
    const cantidadRegistros = arrObjeto.length;
    for (let i = 0; i < cantidadRegistros; i++) {
        const codigo = arrObjeto[i].codTipoUsuario;
        const nombre = arrObjeto[i].nombreTipoUsuario;

        document.getElementById("tablaTipoUsuAdmin").insertRow(-1).innerHTML = "<td>" + codigo + "</td>"
            + "<td>" + nombre + "</td>"
            + "<td class='text-center'><a href='javascript:confirmarEliminar(" + codigo + ");'><i class='fa-solid fa-trash-can check-rojo'></i></a>&nbsp;"
            + "<a href='#utedit/" + codigo + "'><i class='fa-solid fa-pen-to-square'></i></a></td>"
    }
}