function crearTipoUsuario() {
    const nombre = document.getElementById('nom').value;
    if (nombre !== "") {
        let objetoEnviar = {
            nombreTipoUsuario: nombre,
        }
        const apiCrearRol = "http://localhost:8094/backo27g7consultorio/tipousu/crear";
        fetch(apiCrearRol, {
            method: "POST",
            body: JSON.stringify(objetoEnviar),
            headers: { "Content-type": "application/json; charset=UTF-8" }
        })
            .then(respuesta => respuesta.json())
            .then(datos => {
                if (datos.hasOwnProperty('codTipoUsuario')) {
                    console.log(datos)
                    document.getElementById("tuMsgOk").style.display = ""; 
                    document.getElementById("tuMsgError").style.display = "none"; 
                } else {
                    console.log("No se puede grabar -> " + datos.status);
                    document.getElementById("tuMsgOk").style.display = "none"; 
                    document.getElementById("tuMsgError").style.display = ""; 
                }

            })
            .catch(miError => console.log(miError));

        document.getElementById("formaTipoUsuario").reset();
        document.getElementById("formaTipoUsuario").classList.remove("was-validated");
    }
}