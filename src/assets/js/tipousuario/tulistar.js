function obtenerTodosTipoUsuario() {
    const apiObtenerRoles = "http://localhost:8094/backo27g7consultorio/tipousu/obtener";
    const miPromesaRoles = fetch(apiObtenerRoles)
        .then(respuesta => respuesta.json())
        .catch(miError => console.log(miError))

    Promise.all([miPromesaRoles]).then((arregloDatos) => {
        const datos = arregloDatos[0];
        crearFilaTipoUsuListar(datos);
    });
}

function crearFilaTipoUsuListar(arrObjeto) {
    const cantidadRegistros = arrObjeto.length;
    for (let i = 0; i < cantidadRegistros; i++) {
        const codigo = arrObjeto[i].codTipoUsuario;
        const nombre = arrObjeto[i].nombreTipoUsuario;

        document.getElementById("tablaTipoUsuListar").insertRow(-1).innerHTML = "<td>" + codigo + "</td>"
            + "<td>" + nombre + "</td>";
    }
}