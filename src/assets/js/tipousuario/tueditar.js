function editarTipoUsuario() {
    const codigo = document.getElementById('cod').value;
    const nombre = document.getElementById('nom').value;
    if (nombre !== "") {
        let objetoEnviar = {
            codTipoUsuario: codigo,
            nombreTipoUsuario: nombre,
        }
        const apiCrearRol = "http://localhost:8094/backo27g7consultorio/tipousu/editar";
        fetch(apiCrearRol, {
            method: "PUT",
            body: JSON.stringify(objetoEnviar),
            headers: { "Content-type": "application/json; charset=UTF-8" }
        })
            .then(respuesta => respuesta.json())
            .then(datos => {
                // console.log(datos);
                if (datos.error =="Accepted") {
                    // console.log(datos)
                    document.getElementById("tuEditarMsgOk").style.display = "";
                    document.getElementById("tuEditarMsgError").style.display = "none";
                } else {
                    // console.log("No se puede grabar -> " + datos.status);
                    document.getElementById("tuEditarMsgOk").style.display = "none";
                    document.getElementById("tuEditarMsgError").style.display = "";
                }
                // window.location.replace("#utmanage");
            })
            .catch(miError => console.log(miError));

        document.getElementById("formaEditarTipoUsuario").classList.remove("was-validated");
    }

}