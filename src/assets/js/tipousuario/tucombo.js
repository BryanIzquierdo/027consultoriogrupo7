function obtenerTipoUsuarioCombo() {
    const apiObtenerRoles = "http://localhost:8094/consultorioOnline/tipousu/listado";
    const miPromesaRoles = fetch(apiObtenerRoles)
        .then(respuesta => respuesta.json())
        .catch(miError => console.log(miError))

    Promise.all([miPromesaRoles]).then((arregloDatos) => {
        const datos = arregloDatos[0];
        crearElementoSelectTipoUsu(datos);
    });
}

function crearElementoSelectTipoUsu(arrObjeto) {
    const cantidadRegistros = arrObjeto.length;
    const comboTipoUsu = document.getElementById("tipu");
    for (let i = 0; i < cantidadRegistros; i++) {
        const codigo = arrObjeto[i].codTipoUsuario;
        const nombre = arrObjeto[i].nombreTipoUsuario;

        const opcionCombo = document.createElement("option");
        opcionCombo.value = codigo;
        opcionCombo.text = nombre;
        comboTipoUsu.add(opcionCombo);

    }
}