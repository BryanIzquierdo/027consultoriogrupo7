function cargarUnTipoUsuario(codigo) {
    document.getElementById("cod").value = codigo;
    const apiObtenerRoles = "http://localhost:8094/backo27g7consultorio/tipousu/uno/" + codigo;
    fetch(apiObtenerRoles)
        .then((respuesta) => respuesta.json())
        .then((dato) => {
            if (dato.hasOwnProperty('codTipoUsuario')) {
                console.log(dato)
                document.getElementById("nom").value = dato.nombreTipoUsuario;
            } else {
                console.log("No encontrado -> " + dato.status);
                document.getElementById("tuEditarMsgOk").style.display = "none";
                document.getElementById("tuEditarMsgError").style.display = "";
            }
        })
        .catch(miError => console.log(miError));
}