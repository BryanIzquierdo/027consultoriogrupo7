function eliminarTipoUsu(parametro) {

    const apiObtenerRoles = "http://localhost:8094/backo27g7consultorio/tipousu/borrar/" + parametro;
    const miPromesaRoleElim = fetch(apiObtenerRoles, { method: 'DELETE' })
        .then((respuesta) => respuesta.json());

    Promise.all([miPromesaRoleElim]).then((arregloDatos) => {
        const respuestaFinal = arregloDatos[0];
        if (respuestaFinal.status == "200") {
            document.getElementById("alertTipoUsuEliminar").classList.add('alert-primary');
            document.getElementById("msgTipoUsuEliminar").innerHTML = "El tipo usuario " + parametro + " ha sido eliminado con éxito";
        } else {
            document.getElementById("alertTipoUsuEliminar").classList.add('alert-danger');
            document.getElementById("msgTipoUsuEliminar").innerHTML = "El tipo usuario " + parametro + " <b>NO</b> se puede eliminar";
        }
    }
    );
}