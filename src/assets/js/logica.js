function logicaNegocio(url, param) {
    switch (url) {
        // Tipos de usuario
        // ***************************************************************
        case 'src/componentes/tipousuario/tulista.html':
            obtenerTodosTipoUsuario();
            break;
        case 'src/componentes/tipousuario/tuadmin.html':
            obtenerTodosTipoUsuarioAdmin();
            break;
        case 'src/componentes/tipousuario/tueliminar.html':
            eliminarTipoUsu(param);
            break;
        case 'src/componentes/tipousuario/tueditar.html':
            cargarUnTipoUsuario(param);
            break;
        // ***************************************************************
        case 'src/componentes/registro.html':
            obtenerTipoUsuarioCombo();
            document.getElementsByName("validarTodo")[0].onclick = compararClaves;
            break;

        default:
            console.log('Javascript interno no utilizado');
    }

}


